import csv
import os
from pathlib import Path

def formatStringForQuizlet(string: str):
    return string.replace('<br>', '\n').replace('"', '')

if __name__ == "__main__":
    dataBasepath = 'data/'
    outputBasepath = 'output/'
    for entry in os.listdir(dataBasepath):
        if os.path.isfile(os.path.join(dataBasepath, entry)):
            if (entry.endswith('.csv')):
                filename = Path(os.path.basename(entry)).stem

                ankiDeck = []
                with open(dataBasepath + entry, newline='') as csvfile:
                    spamreader = csv.reader(csvfile)
                    for row in spamreader:
                        question = formatStringForQuizlet(row[0])
                        choices = formatStringForQuizlet(row[1])
                        answer = formatStringForQuizlet(row[2])
                        extra = formatStringForQuizlet(row[3])

                        ankiDeck.append([question + '\n\n' + choices, answer + '\n\n' + extra + '@'])

                with open(outputBasepath + filename + '.csv', mode='w') as anki_file:
                    ankiFileWriter = csv.writer(anki_file, delimiter='|', quotechar=' ')
                    for card in ankiDeck:
                        ankiFileWriter.writerow(card)