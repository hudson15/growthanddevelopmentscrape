# 3rd party modules
from bs4 import BeautifulSoup
import requests
# Custom build modules
from AnkiDeck import AnkiDeck
import logincreds
import ankiApi

class Scraper:
    
    session = requests.Session()
    base_URL = 'http://www.orthodonticinstruction.com/modules/'

    def login(self):
        login_URL = 'http://www.orthodonticinstruction.com/index.php'
        response = self.session.post(login_URL, 
        data = {
        'submitCheck' : 1,
        'username': logincreds.username,
        'password' : logincreds.password,
        'submit' : 'Login'})

        return response.status_code

    def getModules(self):
        """must be called after login, returns a LIST of modules"""
        
        page = self.session.get(self.base_URL)
        soup = BeautifulSoup(page.text, "html.parser")
        modules = soup.findAll('a', attrs={'class': "module_link"})

        return modules

    def getXml(self, module_abreviation):
        return self.session.get(f"http://www.orthodonticinstruction.com/modules/modulefiles/dswmedia/{module_abreviation}/data.xml")

    def getModuleIndexRangeForLevel(self, levelNumber: int) -> int:
        if (levelNumber == 1):
            return range(0, 12)
        elif (levelNumber == 2):
            return range(12, 20)
        elif (levelNumber == 3):
            return range(20, 29)
        elif (levelNumber == 4):
            return range(29, 41)
    
    def getDeckName(self, moduleIndex: int) -> str:
        baseName = 'Growth & Development::Module Questions'

        if (moduleIndex in range(0, 12)):
            return baseName + '::Level 1'
        elif (moduleIndex in range(12, 20)):
            return baseName + '::Level 2'
        elif (moduleIndex in range(20, 29)):
            return baseName + '::Level 3'
        elif (moduleIndex in range(29, 41)):
            return baseName + '::Level 4'

if __name__ == "__main__":
    print('Connecting to OrthodonticInstruction.com ...')
    ankiApi.addCustomCardType(title='Prompt / Answer Choices / Answer / Extra',
                          fields=['Prompt', 'Answer Choices', 'Answer', 'Extra'],
                          css=".card {\n font-family: arial;\n font-size: 20px;\n text-align: left;\n color: black;\n background-color: white;\n}\n",
                          cardTemplates=[
                                {
                                    'Front': '{{Prompt}}<br><br>{{ Answer Choices }}',
                                    'Back': '{{ Prompt }}<hr id=answer>{{ Answer Choices }}<hr id=answer>{{ Answer }}<hr id=answer>{{ Extra }}',
                                }
                            ])

    scraper = Scraper()
    scraper.login()
    modules = scraper.getModules()

    xml_url = "http://www.orthodonticinstruction.com/modules/modulefiles/dswmedia/studyphysgrowth/data.xml"
    normal = "http://www.orthodonticinstruction.com/modules/view/1/studyphysgrowth/section/4/page/1"

    getLevelsInput = input("Enter which levels to generate cards for separated by spaces: (Ex. 1 3 4)").split()
    getLevels = set()
    for level in getLevelsInput:
        for index in scraper.getModuleIndexRangeForLevel(levelNumber=int(level)):
            getLevels.add(index)

    print('Thank you! Grabbing data...')

    for moduleIndex, module in enumerate(modules):
        if moduleIndex not in getLevels:
            continue

        print('Starting ' + module.text)

        CARD_TAG = 'Growth&Development::' + module.text.replace(' ', '-').lower()
        module_abbreviation = module["href"].split("/")[-1]
        module_xml = scraper.getXml(module_abbreviation).content
        
        soup = BeautifulSoup(module_xml,'xml')
        
        if soup.find('title', string="Self-Test"):
            self_test_element = soup.find('title', string="Self-Test").parent

        elif soup.find('title', string="Self-test"):
            self_test_element = soup.find('title', string="Self-test").parent

        elif soup.find('title', string="Give Your Recommendations"):
             self_test_element = soup.find('title', string="Give Your Recommendations").parent

        else:
            print('Failed to load the Self-Test for', module.text)
            continue


        questions = self_test_element.find_all("page")


        for page in questions:
           
            
            PROMPT = page.find("pageText").getText()
            
            if(page.find("ol")):
                #assumption based handling of ol
                basic_prompt = page.find("p").getText()
                options_prompt = page.find("ol")
                # potentially useful for quizlet friends

                options_string = '\n'
                option_count = 0
                for index, option in enumerate(options_prompt):
                    option = str(option)
                    if len(option) > 1: # For some reason even when blank, it still has a length of 1
                        option_count = option_count + 1
                        options_string = options_string + f"({ option_count }) { option.replace('<li>', '').replace('</li>', '') }\n"

                PROMPT = f"{basic_prompt} { options_string }"

            choices = page.findAll("choice")
            if(len(choices)==0): continue #Some modules have a residual question talking about textbook chapters
            OPTIONS = list(map(lambda x: x.getText(), choices))

            index_of_correct_choice = page.find("choices")["correctChoice"]
            ANSWER = choices[int(index_of_correct_choice)-1].getText()
            
            #EXTRA = page.find_all('correctResponse')[0].getText() # because I couldnt get this to work
            EXTRA = page.find_all('p') # enter super jank workaround
            EXTRA = EXTRA[1].getText()
            
            # Convert OPTIONS to something viewable on Anki
            letterOptions = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
            assembledOptions = ''

            for index, option in enumerate(OPTIONS):
                assembledOptions = assembledOptions + letterOptions[index] + '. ' + option + '<br>'
            
            #print((PROMPT,OPTIONS,ANSWER,EXTRA,CARD_TAG))

            # Determine deck to load the card into
            myDeck = AnkiDeck(scraper.getDeckName(moduleIndex=moduleIndex))
            myDeck.addCustomCard(model='Prompt / Answer Choices / Answer / Extra', fields={
                    'Prompt': PROMPT,
                    'Answer Choices': assembledOptions,
                    'Answer': ANSWER,
                    'Extra': EXTRA
                }, tags=[CARD_TAG])
            

           
            
    
    


#the last module in level 1 is module #12